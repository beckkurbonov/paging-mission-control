package com.bpd.PagingMissionControl.service;

import com.bpd.PagingMissionControl.model.responseBackToApi;
import org.apache.commons.io.IOUtils;
import org.assertj.core.api.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.junit.jupiter.api.Assertions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

public class PagingMissionControlServiceTest {

  PagingMissionControlService pagingMissionControlService = new PagingMissionControlService();

  @Test
  public void testReadUploadedFile() throws IOException {

    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource("Test1.txt").getFile());

    FileInputStream input = new FileInputStream(file);
    MultipartFile multipartFile =
        new MockMultipartFile("file", file.getName(), "text/plain", IOUtils.toByteArray(input));

    List<responseBackToApi> result = pagingMissionControlService.readUploadedFile(multipartFile);
    Assertions.assertNotNull(result);
  }
}
