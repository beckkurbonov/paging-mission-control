package com.bpd.PagingMissionControl.controller;

import com.bpd.PagingMissionControl.model.responseBackToApi;
import com.bpd.PagingMissionControl.service.PagingMissionControlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
public class PagingMissionControlController {

  @Autowired private PagingMissionControlService pagingMissionControlService;

  public PagingMissionControlController(PagingMissionControlService pagingMissionControlService) {
    this.pagingMissionControlService = pagingMissionControlService;
  }

  @PostMapping(
      path = "/uploadAFile",
      consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
  public List<responseBackToApi> uploadAFile(
      @RequestParam("file") MultipartFile file, ModelMap modelMap) {
    modelMap.addAttribute("file", file);

    return ResponseEntity.ok(pagingMissionControlService.readUploadedFile(file)).getBody();
  }
}
