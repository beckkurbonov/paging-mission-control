package com.bpd.PagingMissionControl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PagingMissionControlApplication {

  public static void main(String[] args) {
    SpringApplication.run(PagingMissionControlApplication.class, args);
  }
}
