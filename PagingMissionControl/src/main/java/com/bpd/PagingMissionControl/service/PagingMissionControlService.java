package com.bpd.PagingMissionControl.service;

import com.bpd.PagingMissionControl.model.convertingOfSatellite;
import com.bpd.PagingMissionControl.model.responseBackToApi;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
public class PagingMissionControlService {

  private static final int INTERVAL = 5;
  private static final int ERROR_THRESHOLD = 3;
  private static final String THERMOSTAT = "TSTAT";
  private static final String BATTERY = "BATT";
  private static final String RED_HIGH = "RED HIGH";
  private static final String RED_LOW = "RED LOW";
  private ArrayList<convertingOfSatellite> currentStates = new ArrayList<>();

  public List<responseBackToApi> readUploadedFile(MultipartFile multipartFile) {
    try (InputStream inputStream = multipartFile.getInputStream()) {
      new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
          .lines()
          .forEach(this::convertingOfSatelliteFromInput);
    } catch (IOException ex) {
      ex.printStackTrace();
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error reading multipartFile");
    }
    List<responseBackToApi> responses = validationOnTheUpload();
    return responses;
  }

  private void convertingOfSatelliteFromInput(String line) {

    convertingOfSatellite parsedState = new convertingOfSatellite();

    try {
      String[] parts = line.split("\\|");

      SimpleDateFormat inputFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
      Date date = inputFormat.parse(parts[0]);
      OffsetDateTime dateTime = date.toInstant().atOffset(ZoneOffset.MIN);

      parsedState.setTimeStamp(dateTime);
      parsedState.setId(parts[1]);
      parsedState.setRedHighLimit(new BigDecimal(parts[2]));
      parsedState.setYellowHighLimit(new BigDecimal(parts[3]));
      parsedState.setYellowLowLimit(new BigDecimal(parts[4]));
      parsedState.setRedLowLimit(new BigDecimal(parts[5]));
      parsedState.setRawValue(new BigDecimal(parts[6]));
      parsedState.setComponent(parts[7]);

      currentStates.add(parsedState);
    } catch (ParseException e) {
      e.printStackTrace();
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error parsing file");
    }
  }

  private List<responseBackToApi> validationOnTheUpload() {
    ArrayList<convertingOfSatellite> reportedInterval = new ArrayList<>();
    ArrayList<responseBackToApi> results = new ArrayList<>();
    currentStates.sort(Comparator.comparing(o -> o.getTimeStamp()));

    for (convertingOfSatellite state : currentStates) {
      OffsetDateTime fiveMinuteInterval = state.getTimeStamp().plusMinutes(INTERVAL);
      ArrayList<convertingOfSatellite> foundWithinInterval = new ArrayList<>();

      if (state.getRawValue().doubleValue() > state.getRedHighLimit().doubleValue()
          && state.getComponent().equals(THERMOSTAT)) {
        for (convertingOfSatellite state2 : currentStates) {
          if (state2.getRawValue().doubleValue() > state2.getRedHighLimit().doubleValue()
              && state2.getComponent().equals(THERMOSTAT)
              && state2.getTimeStamp().isBefore(fiveMinuteInterval)
              && state.getId().equals(state2.getId())
              && !reportedInterval.contains(state2)) {
            foundWithinInterval.add(state2);
            if (foundWithinInterval.size() == ERROR_THRESHOLD) {
              results.add(createResponse(state, RED_HIGH));
              reportedInterval.addAll(foundWithinInterval);
            }
          }
        }
      } else if (state.getRawValue().doubleValue() < state.getRedLowLimit().doubleValue()
          && state.getComponent().equals(BATTERY)) {
        for (convertingOfSatellite state2 : currentStates) {
          if (state2.getRawValue().doubleValue() < state2.getRedLowLimit().doubleValue()
              && state2.getComponent().equals(BATTERY)
              && state2.getTimeStamp().isBefore(fiveMinuteInterval)
              && state.getId().equals(state2.getId())
              && !reportedInterval.contains(state2)) {
            foundWithinInterval.add(state2);
            if (foundWithinInterval.size() == ERROR_THRESHOLD) {
              results.add(createResponse(state, RED_LOW));
              reportedInterval.addAll(foundWithinInterval);
            }
          }
        }
      }
    }
    return results;
  }

  private responseBackToApi createResponse(convertingOfSatellite state, String severity) {
    responseBackToApi response = new responseBackToApi();

    response.setComponent(state.getComponent());
    response.setId(state.getId());
    response.setSeverity(severity);
    response.setTimestamp(String.valueOf(state.getTimeStamp()));

    return response;
  }
}
