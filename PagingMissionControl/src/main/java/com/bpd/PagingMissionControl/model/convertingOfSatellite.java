package com.bpd.PagingMissionControl.model;

import lombok.Data;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Data
public class convertingOfSatellite {

  private OffsetDateTime timeStamp;
  private String id;
  private BigDecimal redHighLimit;
  private BigDecimal redLowLimit;
  private BigDecimal yellowHighLimit;
  private BigDecimal yellowLowLimit;
  private BigDecimal rawValue;
  private String component;
}
