package com.bpd.PagingMissionControl.model;

import lombok.Data;

@Data
public class responseBackToApi {

  private String component;
  private String id;
  private String severity;
  private String timestamp;
}
